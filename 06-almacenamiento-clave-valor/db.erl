-module(db).
-export([new/0, write/3, delete/2, read/2, match/2, destroy/1]).

new() -> [].

write(Key, Element, DbRef) -> [{Key,Element}|DbRef].

delete(Key, DbRef) -> delete(Key, DbRef, []).
delete(Key, [H|T], NewDbRef) ->
    case H of
		  {Key, _} -> delete(Key, T, NewDbRef);
		  _ -> delete(Key, T, [H|NewDbRef])
    end;
delete(_, [], NewDbRef) -> NewDbRef.


read(Key, [H|T]) ->
    case H of
		{Key, Element} -> {ok, Element};
  		_ -> read(Key, T)
	end;
read(_, []) -> {error, instance}.


match(Element, DbRef) -> match(Element, DbRef, []).
match(Element, [H|T], L) ->
	case H of
		{Key, Element} -> match(Element, T, [Key|L]);
		_ -> match(Element, T, L)
	end;
match(_, [], L) -> L.

destroy(_) -> ok.
