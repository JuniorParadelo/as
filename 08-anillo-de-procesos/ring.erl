-module(ring).
-export([start/3, loop/2, start_send/4]).


start_send(PrimerPID, ProcNum, MsgNum, Message)->
    PrimerPID ! {ProcNum, MsgNum, Message, self()},
    receive
        terminated -> ok
    end.



start(ProcNum, MsgNum, Message) when ((ProcNum > 0) and (MsgNum > 0))->
    Pid = self(),
    PrimerPID = spawn(fun() -> loop(Pid, 1) end),
    PrimerPID ! {PrimerPID, ProcNum, MsgNum, Message, 1, self()},
    receive
        finish -> start_send(PrimerPID, ProcNum, MsgNum, Message)
    end;
start(_,_,_) -> {error, ": ProcNum y MsgNum tienen que ser mayores de 0"}.




loop(SiguientePID,Numero)->
    receive
        {PrimerPID, ProcNum, _, _, ProcNum, Return}->
            Return ! finish,
            loop(PrimerPID, ProcNum);
        {PrimerPID, ProcNum, MsgNum, Message, N, Return}->
            PidN = spawn(fun() -> loop(self(), N) end),
            PidN ! {PrimerPID, ProcNum, MsgNum, Message, N+1, Return},
            loop(PidN, N);
        {ProcNum, 1, _, ExitPid}->
            SiguientePID ! {stop, ProcNum, ExitPid},
            loop(SiguientePID, Numero);
        {ProcNum, MsgNum, Message, ExitPid} -> 
            SiguientePID ! {ProcNum, MsgNum-1, Message, ExitPid},
            loop(SiguientePID, Numero);
        {stop, 1, ExitPid} ->
            ExitPid ! terminated;
        {stop, ProcNum, ExitPid} ->
            SiguientePID ! {stop, ProcNum-1, ExitPid}
    end.

