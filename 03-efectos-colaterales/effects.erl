-module(effects).
-export([print/1, even_print/1]).

suma(Acc, N) when Acc<N ->
	io:format("~p ", [Acc]) , suma(Acc+1, N);
suma(_, N) -> N.

print(N) when N>0 -> suma(1,N).




resto(Acc, N) when N>=Acc ->
	case Acc rem 2 of
		0 -> io:format("~p ", [Acc]) , resto(Acc+1, N);
		_ -> resto(Acc+1, N)
	end;
resto(Acc, N) when Acc>N -> io:format("~n").

even_print(N) -> resto(2, N).