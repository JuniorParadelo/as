-module(create).
-export([create/1, reverse_create/1]).


lista_asc(N, Acc) ->
	case Acc =< N of
		true -> [Acc | lista_asc(N, Acc+1)];
		false -> []
	end.

create(N) when N>0 -> 
	lista_asc(N, 1).



reverse_create(N) when N>0 ->
	[N | reverse_create(N-1)];
reverse_create(_) -> [].