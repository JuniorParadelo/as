-module(manipulating).
-export([filter/2, reverse/1, concatenate/1, flatten/1]).


filter(L,N) -> [X || X <- L, N >= X].



reverse(L) -> reverse(L, []).

reverse([], Acc) -> 
	Acc;
reverse([H|T], Acc) -> 
	reverse(T, [H|Acc]).



concatenate(L) ->
    concatenate(L, []).
concatenate([], Acc) ->
    Acc;
concatenate([H|T], Acc) ->
    concatenate(T, Acc ++ H).


flatten(L) -> 
	reverse(flatten(L,[])).
flatten([],Acc) -> 
	Acc;
flatten([H|T], Acc) when is_list(H) -> 
	flatten(T, flatten(H, Acc));
flatten([H|T],Acc) -> 
	flatten(T, [H|Acc]).

