-module(sorting).
-export([quicksort/1, mergesort/1]).


quicksort([]) -> 
	[];
quicksort([Pivote|T]) ->
	quicksort([X || X <- T, X < Pivote]) ++ [Pivote | quicksort([X || X <- T, X >= Pivote])].



merge(L1, []) -> L1;
merge([], L2) -> L2;
merge([HeadA|TailA], [HeadB|TailB]) ->
  if
    HeadA < HeadB -> [HeadA | merge(TailA, [HeadB|TailB])];
    true -> [HeadB | merge([HeadA|TailA], TailB)]
  end.


mergesort([]) -> [];
mergesort([E]) -> [E];
mergesort(L) ->
    {L1 , L2} = lists:split(trunc(length(L)/2), L),
  	merge(mergesort(L1), mergesort(L2)).
