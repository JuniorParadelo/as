-module(echo).

-export([start/0, print/1, stop/0]).


start() -> 
	register(?MODULE, spawn(fun loop/0)),
	ok.

print(Term) ->
	?MODULE ! {print, Term},
	ok.


loop() ->
	receive
		stop ->
			{ok, self()};
		{print, Term} ->
			io:format("~p ~n", [Term]),
			loop();
		_ -> 
			loop(),
			ok
	end.


stop() ->
	?MODULE ! stop,
    unregister(echo),
    ok.